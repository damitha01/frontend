import Vue from 'vue'
import Router from 'vue-router'

import Login from '../views/Login.vue'
import Customers from '../views/Customers.vue'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      redirect: {
        name: 'customers'
      }
    },
    {
      path: '/login',
      name: 'login',
      component: Login,
      meta: { layout: 'loginlayout' }
    },
    {
      path: '/customers',
      name: 'customers',
      component: Customers,
      meta: { layout: 'defaultlayout' }
    }
  ]
})
