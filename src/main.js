// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'

import DefaultLayout from './components/layouts/DefaultLayout'
import LoginLayout from './components/layouts/LoginLayout'

// register jw pagination component globally
import JwPagination from 'jw-vue-pagination';

Vue.config.productionTip = false

Vue.component('defaultlayout', DefaultLayout)
Vue.component('loginlayout', LoginLayout)
Vue.component('jw-pagination', JwPagination);

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
})
