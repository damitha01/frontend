# ABC Customer Relationship Management System
This is a test Project for Customer Relationship Management System.
This Consist of two modules as mentioned below.

  - Frontend (VUE JS App)
  - Backend (Laravel - LUMEN API App)

### Frontend (VUE JS App) - Frontend

Frontend uses a Vue Js Single Page app. This uses below technologies to fulfill its needs.

* [Vue JS] - HTML enhanced for web apps!
* [Bootsrap 5] - CSS web UI framework
* [Node Modules] - Runtime and development support

### Backend (Laravel - LUMEN API App) - API

Backend uses a Lumen based Laravel framwork to use a lightweighted app to handle API requests effienlty. From backend its getting and sending frontend API responses and process through the DB.
### Installation
Before proceeding to the below command need to install composer. https://getcomposer.org/download/

Goto the backend folder "api" and open the command terminal and run below code.

```sh
$ cd api
$ composer install
```
Create a database and modify the ".env" file and modifiy the database config details relatively.

Then run migration for updating the database tables

```sh
$ php artisan migrate
$ php artisan db:seed --class=AdminSeeder
```
This will Upload database tables and Admin user data to Users table
ADMIN Credentials are as below

Username: adminuser@gmail.com
Password: Adminuser@123

Then run below code for creating the JWT Token to the api authentication process

```sh
$ php artisan jwt:secret
```

when need to start the backend run code below

```sh
$ php -S localhost:8000 -t public
```
This (localhost:8000) is the URL which uses from the Frontend to run the APIs.

Then goto the Frontend folder "frontend" and open the terminal and run below command to config enviorenment

```sh
$ npm install
```

To start the frontend run below command.
```sh
$ npm run dev
```